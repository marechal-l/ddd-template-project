package domain.booking.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class GetBookingCommand {

    private String roomId;
    //Optional
    private String userEmail;

    public GetBookingCommand(String roomId){
        this.roomId = roomId;
    }
}
