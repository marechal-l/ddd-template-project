package domain.booking.error;

public class BookingWrongOwner extends RuntimeException{

    public BookingWrongOwner(String message){
        super(message);
    }
}
