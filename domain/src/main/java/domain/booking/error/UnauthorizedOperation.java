package domain.booking.error;

public class UnauthorizedOperation extends RuntimeException{

    public UnauthorizedOperation(String message){
        super(message);
    }
}
