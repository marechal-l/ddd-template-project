package domain.booking.error;

public class BookingAlreadyExists extends RuntimeException{

    public BookingAlreadyExists(String message){
        super(message);
    }
}
