package application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"infrastructure", "usecase"})
public class Application {
    public static void main(String[] args){
        System.out.println("Hello boot ");
        SpringApplication.run(Application.class, args);
    }
}
