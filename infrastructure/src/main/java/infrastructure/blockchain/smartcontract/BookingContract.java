package infrastructure.blockchain.smartcontract;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Bytes32;
import org.web3j.abi.datatypes.generated.Uint8;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import rx.Observable;
import rx.functions.Func1;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.3.1.
 */
public class BookingContract extends Contract {
    private static final String BINARY = "0x608060405234801561001057600080fd5b50336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16600073ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a36100e533610703640100000000026401000000009004565b5060038060018154018082558091505090600182039060005260206000200160007f43303100000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43303200000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43303300000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43303400000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43303500000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43303600000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43303700000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43303800000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43303900000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f43313000000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303100000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303200000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303300000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303400000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303500000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303600000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303700000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303800000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f50303900000000000000000000000000000000000000000000000000000000009091909150906000191690555060038060018154018082558091505090600182039060005260206000200160007f503130000000000000000000000000000000000000000000000000000000000090919091509060001916905550610895565b600061071c61083e640100000000026401000000009004565b151561072757600080fd5b600160008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615156108395760018060008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff0219169083151502179055507fd1bba68c128cc3f427e5831b3c6f99f480b6efa6b9e80c757768f6124158cc3f82604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390a1600190505b919050565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614905090565b611514806108a46000396000f3006080604052600436106100e6576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680631bae0ac8146100eb57806324953eaa14610134578063286dd3f5146101b25780636a51cbfc1461020d578063715018a6146102a15780637b9417c8146102b85780638d5d34dc146103135780638da5cb5b1461032a5780638f32d59b146103815780639b19251a146103b0578063bb0173251461040b578063d0b416f714610491578063dffe6f21146104dd578063e2ec6ec314610529578063f2fde38b146105a7578063f794cf2d146105ea575b600080fd5b3480156100f757600080fd5b5061011660048036038101908080359060200190929190505050610656565b60405180826000191660001916815260200191505060405180910390f35b34801561014057600080fd5b5061019860048036038101908080359060200190820180359060200190808060200260200160405190810160405280939291908181526020018383602002808284378201915050505050509192919290505050610679565b604051808215151515815260200191505060405180910390f35b3480156101be57600080fd5b506101f3600480360381019080803573ffffffffffffffffffffffffffffffffffffffff1690602001909291905050506106da565b604051808215151515815260200191505060405180910390f35b34801561021957600080fd5b5061024a60048036038101908080356000191690602001909291908035600019169060200190929190505050610806565b6040518080602001828103825283818151815260200191508051906020019060200280838360005b8381101561028d578082015181840152602081019050610272565b505050509050019250505060405180910390f35b3480156102ad57600080fd5b506102b661093d565b005b3480156102c457600080fd5b506102f9600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610a0f565b604051808215151515815260200191505060405180910390f35b34801561031f57600080fd5b50610328610b3b565b005b34801561033657600080fd5b5061033f610cd1565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34801561038d57600080fd5b50610396610cfa565b604051808215151515815260200191505060405180910390f35b3480156103bc57600080fd5b506103f1600480360381019080803573ffffffffffffffffffffffffffffffffffffffff169060200190929190505050610d51565b604051808215151515815260200191505060405180910390f35b34801561041757600080fd5b5061043a6004803603810190808035600019169060200190929190505050610d71565b6040518080602001828103825283818151815260200191508051906020019060200280838360005b8381101561047d578082015181840152602081019050610462565b505050509050019250505060405180910390f35b34801561049d57600080fd5b506104db6004803603810190808035600019169060200190929190803560ff1690602001909291908035600019169060200190929190505050610e5f565b005b3480156104e957600080fd5b506105276004803603810190808035600019169060200190929190803560ff169060200190929190803560001916906020019092919050505061101a565b005b34801561053557600080fd5b5061058d600480360381019080803590602001908201803590602001908080602002602001604051908101604052809392919081815260200183836020028082843782019150505050505091929192905050506111ec565b604051808215151515815260200191505060405180910390f35b3480156105b357600080fd5b506105e8600480360381019080803573ffffffffffffffffffffffffffffffffffffffff16906020019092919050505061124d565b005b3480156105f657600080fd5b506105ff61126c565b6040518080602001828103825283818151815260200191508051906020019060200280838360005b83811015610642578082015181840152602081019050610627565b505050509050019250505060405180910390f35b60038181548110151561066557fe5b906000526020600020016000915090505481565b600080610684610cfa565b151561068f57600080fd5b600090505b82518110156106d4576106bd83828151811015156106ae57fe5b906020019060200201516106da565b156106c757600191505b8080600101915050610694565b50919050565b60006106e4610cfa565b15156106ef57600080fd5b600160008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff1615610801576000600160008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff0219169083151502179055507ff1abf01a1043b7c244d128e8595cf0c1d10743b022b03a02dffd8ca3bf729f5a82604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390a1600190505b919050565b606080600080601760ff1660405190808252806020026020018201604052801561083f5781602001602082028038833980820191505090505b50925060009150600090505b601760ff168160ff1610156109285760026000876000191660001916815260200190815260200160002060008260ff1660ff16815260200190815260200160002060010160009054906101000a900460ff1680156108e55750846000191660026000886000191660001916815260200190815260200160002060008360ff1660ff1681526020019081526020016000206000015460001916145b1561091b5780838360ff168151811015156108fc57fe5b9060200190602002019060ff16908160ff168152505081806001019250505b808060010191505061084b565b61093283836112c8565b935050505092915050565b610945610cfa565b151561095057600080fd5b600073ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a360008060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550565b6000610a19610cfa565b1515610a2457600080fd5b600160008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff161515610b365760018060008473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff0219169083151502179055507fd1bba68c128cc3f427e5831b3c6f99f480b6efa6b9e80c757768f6124158cc3f82604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390a1600190505b919050565b600080610b46610cfa565b1515610b5157600080fd5b600091505b6003805490508260ff161015610ccd57600090505b601760ff168160ff161015610cc0576002600060038460ff16815481101515610b9057fe5b90600052602060002001546000191660001916815260200190815260200160002060008260ff1660ff16815260200190815260200160002060010160009054906101000a900460ff1615610cb35760405180600001905060405180910390206002600060038560ff16815481101515610c0557fe5b90600052602060002001546000191660001916815260200190815260200160002060008360ff1660ff168152602001908152602001600020600001816000191690555060006002600060038560ff16815481101515610c6057fe5b90600052602060002001546000191660001916815260200190815260200160002060008360ff1660ff16815260200190815260200160002060010160006101000a81548160ff0219169083151502179055505b8080600101915050610b6b565b8180600101925050610b56565b5050565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614905090565b60016020528060005260406000206000915054906101000a900460ff1681565b606080600080601760ff16604051908082528060200260200182016040528015610daa5781602001602082028038833980820191505090505b50925060009150600090505b601760ff168160ff161015610e4b5760026000866000191660001916815260200190815260200160002060008260ff1660ff16815260200190815260200160002060010160009054906101000a900460ff1615610e3e5780838360ff16815181101515610e1f57fe5b9060200190602002019060ff16908160ff168152505081806001019250505b8080600101915050610db6565b610e5583836112c8565b9350505050919050565b600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff161515610eb757600080fd5b600060ff168260ff1610158015610ed55750601760ff168260ff1611155b1515610ee057600080fd5b60026000846000191660001916815260200190815260200160002060008360ff1660ff16815260200190815260200160002060010160009054906101000a900460ff161515610f2e57600080fd5b806000191660026000856000191660001916815260200190815260200160002060008460ff1660ff1681526020019081526020016000206000015460001916141515610f7957600080fd5b604051806000019050604051809103902060026000856000191660001916815260200190815260200160002060008460ff1660ff1681526020019081526020016000206000018160001916905550600060026000856000191660001916815260200190815260200160002060008460ff1660ff16815260200190815260200160002060010160006101000a81548160ff021916908315150217905550505050565b6110226114c9565b600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151561107a57600080fd5b600060ff168360ff16101580156110985750601760ff168360ff1611155b15156110a357600080fd5b60026000856000191660001916815260200190815260200160002060008460ff1660ff16815260200190815260200160002060010160009054906101000a900460ff161515156110f257600080fd5b611150600380548060200260200160405190810160405280929190818152602001828054801561114557602002820191906000526020600020905b8154600019168152602001906001019080831161112d575b50505050508561136f565b151561115b57600080fd5b818160000190600019169081600019168152505060018160200190151590811515815250508060026000866000191660001916815260200190815260200160002060008560ff1660ff1681526020019081526020016000206000820151816000019060001916905560208201518160010160006101000a81548160ff02191690831515021790555090505050505050565b6000806111f7610cfa565b151561120257600080fd5b600090505b825181101561124757611230838281518110151561122157fe5b90602001906020020151610a0f565b1561123a57600191505b8080600101915050611207565b50919050565b611255610cfa565b151561126057600080fd5b611269816113cf565b50565b606060038054806020026020016040519081016040528092919081815260200182805480156112be57602002820191906000526020600020905b815460001916815260200190600101908083116112a6575b5050505050905090565b60608060008360ff166040519080825280602002602001820160405280156112ff5781602001602082028038833980820191505090505b509150600090505b8360ff168160ff16101561136457848160ff1681518110151561132657fe5b90602001906020020151828260ff1681518110151561134157fe5b9060200190602002019060ff16908160ff16815250508080600101915050611307565b819250505092915050565b600080600090505b83518160ff1610156113c3578260001916848260ff1681518110151561139957fe5b906020019060200201516000191614156113b657600191506113c8565b8080600101915050611377565b600091505b5092915050565b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff161415151561140b57600080fd5b8073ffffffffffffffffffffffffffffffffffffffff166000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a3806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555050565b60408051908101604052806000801916815260200160001515815250905600a165627a7a7230582018f51f1c194f28e87286692eb6f1a5f155973abe2fa1f9ffcef3a8b08971dc890029";

    protected static final HashMap<String, String> _addresses;

    static {
        _addresses = new HashMap<>();
        _addresses.put("4", "0xf7396a3542e8ed994fb1a29d85235f1b6d4bedc7");
    }

    protected BookingContract(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected BookingContract(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public List<WhitelistedAddressAddedEventResponse> getWhitelistedAddressAddedEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("WhitelistedAddressAdded", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(event, transactionReceipt);
        ArrayList<WhitelistedAddressAddedEventResponse> responses = new ArrayList<WhitelistedAddressAddedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            WhitelistedAddressAddedEventResponse typedResponse = new WhitelistedAddressAddedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.addr = (String) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<WhitelistedAddressAddedEventResponse> whitelistedAddressAddedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("WhitelistedAddressAdded", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, WhitelistedAddressAddedEventResponse>() {
            @Override
            public WhitelistedAddressAddedEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(event, log);
                WhitelistedAddressAddedEventResponse typedResponse = new WhitelistedAddressAddedEventResponse();
                typedResponse.log = log;
                typedResponse.addr = (String) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public List<WhitelistedAddressRemovedEventResponse> getWhitelistedAddressRemovedEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("WhitelistedAddressRemoved", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(event, transactionReceipt);
        ArrayList<WhitelistedAddressRemovedEventResponse> responses = new ArrayList<WhitelistedAddressRemovedEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            WhitelistedAddressRemovedEventResponse typedResponse = new WhitelistedAddressRemovedEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.addr = (String) eventValues.getNonIndexedValues().get(0).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<WhitelistedAddressRemovedEventResponse> whitelistedAddressRemovedEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("WhitelistedAddressRemoved", 
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, WhitelistedAddressRemovedEventResponse>() {
            @Override
            public WhitelistedAddressRemovedEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(event, log);
                WhitelistedAddressRemovedEventResponse typedResponse = new WhitelistedAddressRemovedEventResponse();
                typedResponse.log = log;
                typedResponse.addr = (String) eventValues.getNonIndexedValues().get(0).getValue();
                return typedResponse;
            }
        });
    }

    public List<OwnershipTransferredEventResponse> getOwnershipTransferredEvents(TransactionReceipt transactionReceipt) {
        final Event event = new Event("OwnershipTransferred", 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}),
                Arrays.<TypeReference<?>>asList());
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(event, transactionReceipt);
        ArrayList<OwnershipTransferredEventResponse> responses = new ArrayList<OwnershipTransferredEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
            typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Observable<OwnershipTransferredEventResponse> ownershipTransferredEventObservable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        final Event event = new Event("OwnershipTransferred", 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}, new TypeReference<Address>() {}),
                Arrays.<TypeReference<?>>asList());
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(event));
        return web3j.ethLogObservable(filter).map(new Func1<Log, OwnershipTransferredEventResponse>() {
            @Override
            public OwnershipTransferredEventResponse call(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(event, log);
                OwnershipTransferredEventResponse typedResponse = new OwnershipTransferredEventResponse();
                typedResponse.log = log;
                typedResponse.previousOwner = (String) eventValues.getIndexedValues().get(0).getValue();
                typedResponse.newOwner = (String) eventValues.getIndexedValues().get(1).getValue();
                return typedResponse;
            }
        });
    }

    public RemoteCall<byte[]> rooms(BigInteger param0) {
        final Function function = new Function("rooms", 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.generated.Uint256(param0)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bytes32>() {}));
        return executeRemoteCallSingleValueReturn(function, byte[].class);
    }

    public RemoteCall<TransactionReceipt> removeAddressesFromWhitelist(List<String> addrs) {
        final Function function = new Function(
                "removeAddressesFromWhitelist", 
                Arrays.<Type>asList(new DynamicArray<Address>(
                        org.web3j.abi.Utils.typeMap(addrs, Address.class))),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> removeAddressFromWhitelist(String addr) {
        final Function function = new Function(
                "removeAddressFromWhitelist", 
                Arrays.<Type>asList(new Address(addr)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> renounceOwnership() {
        final Function function = new Function(
                "renounceOwnership", 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> addAddressToWhitelist(String addr) {
        final Function function = new Function(
                "addAddressToWhitelist", 
                Arrays.<Type>asList(new Address(addr)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<String> owner() {
        final Function function = new Function("owner", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }

    public RemoteCall<Boolean> isOwner() {
        final Function function = new Function("isOwner", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<Boolean> whitelist(String param0) {
        final Function function = new Function("whitelist", 
                Arrays.<Type>asList(new Address(param0)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteCall<TransactionReceipt> addAddressesToWhitelist(List<String> addrs) {
        final Function function = new Function(
                "addAddressesToWhitelist", 
                Arrays.<Type>asList(new DynamicArray<Address>(
                        org.web3j.abi.Utils.typeMap(addrs, Address.class))),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> transferOwnership(String newOwner) {
        final Function function = new Function(
                "transferOwnership", 
                Arrays.<Type>asList(new Address(newOwner)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static RemoteCall<BookingContract> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(BookingContract.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<BookingContract> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(BookingContract.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public RemoteCall<TransactionReceipt> clearAllBookings() {
        final Function function = new Function(
                "clearAllBookings", 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> addBooking(byte[] roomId, BigInteger timeSlot, byte[] userId) {
        final Function function = new Function(
                "addBooking", 
                Arrays.<Type>asList(new Bytes32(roomId),
                new Uint8(timeSlot),
                new Bytes32(userId)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<TransactionReceipt> cancelBooking(byte[] roomId, BigInteger timeSlot, byte[] userId) {
        final Function function = new Function(
                "cancelBooking", 
                Arrays.<Type>asList(new Bytes32(roomId),
                new Uint8(timeSlot),
                new Bytes32(userId)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<List> getBookings(byte[] roomId) {
        final Function function = new Function("getBookings", 
                Arrays.<Type>asList(new Bytes32(roomId)),
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Uint8>>() {}));
        return new RemoteCall<List>(
                new Callable<List>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    public List call() throws Exception {
                        List<Type> result = (List<Type>) executeCallSingleValueReturn(function, List.class);
                        return convertToNative(result);
                    }
                });
    }

    public RemoteCall<List> getUserBookings(byte[] roomId, byte[] userId) {
        final Function function = new Function("getUserBookings", 
                Arrays.<Type>asList(new Bytes32(roomId),
                new Bytes32(userId)),
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Uint8>>() {}));
        return new RemoteCall<List>(
                new Callable<List>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    public List call() throws Exception {
                        List<Type> result = (List<Type>) executeCallSingleValueReturn(function, List.class);
                        return convertToNative(result);
                    }
                });
    }

    public RemoteCall<List> getAllRooms() {
        final Function function = new Function("getAllRooms", 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Bytes32>>() {}));
        return new RemoteCall<List>(
                new Callable<List>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    public List call() throws Exception {
                        List<Type> result = (List<Type>) executeCallSingleValueReturn(function, List.class);
                        return convertToNative(result);
                    }
                });
    }

    public static BookingContract load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new BookingContract(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static BookingContract load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new BookingContract(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected String getStaticDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static String getPreviouslyDeployedAddress(String networkId) {
        return _addresses.get(networkId);
    }

    public static class WhitelistedAddressAddedEventResponse {
        public Log log;

        public String addr;
    }

    public static class WhitelistedAddressRemovedEventResponse {
        public Log log;

        public String addr;
    }

    public static class OwnershipTransferredEventResponse {
        public Log log;

        public String previousOwner;

        public String newOwner;
    }
}
