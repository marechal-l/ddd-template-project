package infrastructure.blockchain.wallet;

import lombok.Getter;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

@Getter
public class WalletManager {

    //Random values
    private static final String INFURA_API_KEY = "01020304";
    private static final String PRIVATE_KEY = "01020304";

    private Web3j web3j;
    private Credentials credentials;

    public WalletManager(){
        web3j = Web3j.build(new HttpService("https://rinkeby.infura.io/v3/" + INFURA_API_KEY));
        credentials = Credentials.create(PRIVATE_KEY);
    }
}
