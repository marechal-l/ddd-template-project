package infrastructure.blockchain.network;

import domain.booking.blockchain.FutureCallback;

import java.util.concurrent.Future;

public class WaitTransaction<T> {

    public static <T> void waitForFuture(final Future<T> future, final FutureCallback callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    do {
                        Thread.sleep(1000);
                        System.out.println("Wait transaction");
                    } while (!future.isDone());
                    callback.isDone(future);
                } catch (Exception e) {
                    System.out.println("onTransfer : " + e.getMessage());
                }
            }
        }).start();
    }
}
