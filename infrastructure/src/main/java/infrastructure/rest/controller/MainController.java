package infrastructure.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import infrastructure.rest.Mapping;
import infrastructure.rest.model.response.HealthResponse;

@RestController
@RequestMapping("/")
public class MainController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @RequestMapping("/")
    public String applyRoot() {
        LOGGER.info("Applying "+" / " +" without args");
        return "Hello I am g_root";
    }

    @RequestMapping(infrastructure.rest.Mapping.HEALTH)
    @ResponseBody
    public HealthResponse apply() {
        LOGGER.info("Applying "+ Mapping.HEALTH +" without args");
        return new HealthResponse();
    }
}
