package infrastructure.rest.controller;

import domain.booking.command.AddBookingCommand;
import domain.booking.command.CancelBookingCommand;
import domain.booking.command.GetBookingCommand;
import infrastructure.rest.model.request.GetBookingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import infrastructure.rest.Mapping;
import infrastructure.rest.model.request.AddBookingRequest;
import infrastructure.rest.model.request.CancelBookingRequest;
import usecase.BookingService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(Mapping.Booking.ROOT_BOOKING)
public class BookingController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingController.class);

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping(Mapping.Booking.ADD_BOOKING)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public String apply(@Valid @RequestBody AddBookingRequest request) {
        LOGGER.info("Applying "+ infrastructure.rest.Mapping.Booking.ADD_BOOKING +" with args {}",request);
        return bookingService.addBooking(AddBookingCommand.builder()
                .roomId(request.getRoomId())
                .userEmail(request.getUserEmail())
                .timeSlot(request.getTimeSlot())
                .build());
    }

    @PostMapping(Mapping.Booking.CANCEL_BOOKING)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public String apply(@Valid @RequestBody CancelBookingRequest request) {
        LOGGER.info("Applying "+ Mapping.Booking.CANCEL_BOOKING +" with args {}",request);
        return bookingService.cancelBooking(CancelBookingCommand.builder()
                .roomId(request.getRoomId())
                .userEmail(request.getUserEmail())
                .timeSlot(request.getTimeSlot())
                .build());

    }

    @PostMapping(Mapping.Booking.GET_BOOKINGS)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<Integer> apply(@Valid @RequestBody GetBookingRequest request) {
        LOGGER.info("Applying "+ Mapping.Booking.GET_BOOKINGS +" with args {}",request);
        return bookingService.getBookings(GetBookingCommand.builder()
                .roomId(request.getRoomId())
                .userEmail(request.getUserEmail())
                .build());
    }

    @GetMapping(Mapping.Booking.GET_ROOMS)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<List<String>> apply() {
        LOGGER.info("Applying "+ Mapping.Booking.GET_ROOMS);
        return new ResponseEntity<>(bookingService.getRooms(), HttpStatus.OK);
    }
}
