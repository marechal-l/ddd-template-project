package infrastructure.rest.model.response;

import lombok.Getter;

@Getter
public class HealthResponse {

    private String version;
    private String name;
    private String message;

    public HealthResponse(){
        version = "1.0-SNAPSHOT";
        name = "Booking Service";
        message = "This service is ready to handle any of your request";
    }
}
