package infrastructure.rest.model.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class CancelBookingRequest {
    @NotNull
    @Email
    private String userEmail;

    @NotNull
    @Size(min = 3, max = 3)
    private String roomId;

    @NotNull
    @Pattern(regexp = "([01]?[0-9]|2[0-3])")
    private String timeSlot;
}


